import { isColorEqual } from '../../src/utils';

// given Uint8ClampedArray of all pixel data
const imageData = new Uint8ClampedArray([
  // first pixel
  0, 0, 0, 255,
  // second pixel
  255, 255, 255, 255
]);
// black is the target color
const targetColor = [0, 0, 0, 255];

describe('isColorEqual', () => {
  it('correctly compares color for the first pixel', () => {
    const result = isColorEqual(imageData, 0, targetColor);
    expect(result).toBe(true);
  });

  it('correctly compares color for the second pixel', () => {
    const result = isColorEqual(imageData, 4, targetColor);
    expect(result).toBe(false);
  });
});
