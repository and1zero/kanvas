import { hexToRgb } from '../../src/utils';

describe('hexToRgb', () => {
  [
    ['#FFF', [255, 255, 255, 255]],
    ['#000', [0, 0, 0, 255]],
    ['#F00', [255, 0, 0, 255]],
    ['#CCCCCC', [204, 204, 204, 255]],
    ['any invalid string', [255, 255, 255, 255]]
  ].forEach(([input, output]) => {
    it(`converts ${input} to ${output}`, () => {
      expect(hexToRgb(input)).toEqual(new Uint8ClampedArray(output));
    });
  });
});
