import {
  COLOR_GREY,
  COLOR_TRANSPARENT,
  COLOR_WHITE,
  PIXEL_SIZE,
  fillImageData,
  getImageDataIndex,
  drawLineImageData
} from '../../src/utils';

const width = 22;
const height = 6;
const backgroundColor = COLOR_TRANSPARENT; // transparent black
const strokeColor = COLOR_WHITE; // white
const fillColor = COLOR_GREY; // grey
// mock canvas 2d context from drawing_program
let imageData = {
  width: width,
  height: height,
  // create canvas with black transparent background
  data: new Uint8ClampedArray(width * height * PIXEL_SIZE)
};

const getColor = (x, y) => {
  const index = getImageDataIndex(width, x, y);
  return imageData.data.slice(index, index + PIXEL_SIZE);
}

// set the pixel values based on Bresenham's line algorithm
const drawLine = (x1, y1, x2, y2) => {
  drawLineImageData(imageData, x1, y1, x2, y2, strokeColor);
}

const drawRect = (x1, y1, x2, y2) => {
  drawLine(x1, y1, x2, y1);
  drawLine(x2, y1, x2, y2);
  drawLine(x2, y2, x1, y2);
  drawLine(x1, y2, x1, y1);
}

describe('fillImageData', () => {
  beforeEach(() => {
    // reset background
    imageData.data = new Uint8ClampedArray(width * height * PIXEL_SIZE);
    // draw the canvas (with border)
    drawRect(0, 0, 21, 5);
    // draw lines and rectangle
    drawLine(1, 2, 6, 2);
    drawLine(6, 2, 6, 4);
    drawRect(14, 1, 18, 3);
  });

  describe('with similar color', () => {
    beforeEach(() => {
      fillImageData(imageData, 10, 3, backgroundColor);
    });

    it('does nothing', () => {
      const color = getColor(10, 3);
      expect(color).toEqual(backgroundColor);
    });
  });

  describe('with different color', () => {
    beforeEach(() => {
      fillImageData(imageData, 10, 3, fillColor);
    });

    [
      [10, 3],
      [1, 1],
      [2, 1],
      [3, 1],
      [4, 1],
      [5, 1],
      [6, 1],
      [19, 1],
      [19, 2],
      [19, 3]
    ].forEach(([x, y]) => {
      it(`fills areas (${x}, ${y})`, () => {
        const color = getColor(x, y);
        expect(color).toEqual(fillColor);
      });
    });

    it('fills selected pixel', function() {
      const color = getColor(10, 3);
      expect(color).toEqual(fillColor);
    });

    [
      [1, 2],
      [2, 2],
      [3, 2],
      [4, 2],
      [5, 2],
      [6, 2]
    ].forEach(([x, y]) => {
      it(`does not fill line pixels stroke (${x}, ${y})`, () => {
        const color = getColor(x, y);
        expect(color).toEqual(strokeColor);
      });
    });

    [
      [14, 1],
      [15, 1],
      [16, 1],
      [17, 1],
      [18, 1],
      [18, 2],
      [18, 3],
      [17, 3],
      [16, 3],
      [15, 3],
      [14, 3],
      [14, 2]
    ].forEach(([x, y]) => {
      it(`does not fill rectangle pixels stroke (${x}, ${y})`, () => {
        const color = getColor(x, y);
        expect(color).toEqual(strokeColor);
      });
    });

    [
      [1, 3],
      [2, 3],
      [3, 3],
      [4, 3],
      [5, 3]
    ].forEach(([x, y]) => {
      it(`does not fill area between lines and canvas (${x}, ${y})`, () => {
        const color = getColor(x, y);
        expect(color).toEqual(backgroundColor);
      });
    });

    [
      [15, 2],
      [16, 2],
      [17, 2]
    ].forEach(([x, y]) => {
      it(`does not fill area in rectangle (${x}, ${y})`, () => {
        const color = getColor(x, y);
        expect(color).toEqual(backgroundColor);
      });
    });
  });
});
