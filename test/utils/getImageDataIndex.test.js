import { getImageDataIndex } from '../../src/utils';

const array = new Uint8ClampedArray(200);

describe('getImageDataIndex', () => {
  it('returns correct index position', () => {
    const index = getImageDataIndex(20, 10, 5, 1);// 20x10 pixels
    expect(index).toEqual(110);
  });

  it('returns correct index position for custom pixel size', () => {
    const index = getImageDataIndex(10, 5, 5, 2); // 10x10 pixels
    expect(index).toEqual(110);;
  });
});
