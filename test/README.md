## Testing

``` bash
$ npm test (or yarn test)
```

We are using [Jest](https://facebook.github.io/jest/) for testing and code coverage. Coverage will be generated in `coverage` folder.

```bash
$ open coverage/lcov-report/index.html
```

Currently what we have covered in testing:
- [x] [Utilities] converting hex color into Array of RGBA.
- [x] [Utilities] comparing 2 colors given the imageData object of canvas.
- [x] [Utilities] fill the canvas with flood_fill algorithm.
- [x] [Utilities] converts coordinate into index in Uint8ClampedArray of imageData.
- [x] Console. Testing all kinds of inputs.
- [x] Canvas.

## Disclaimer

Please make sure to run `$ npm (or yarn) install` before running all the tests.
