/**
 * @jest-environment jsdom
 */
import Canvas from '../src/Canvas';

describe('Canvas', () => {
  it('can generate context', () => {
    const kanvas = new Canvas();
    const ctx = kanvas.ctx;
    expect(kanvas.element.width).toEqual(0);
    expect(kanvas.element.height).toEqual(0);
    expect(kanvas.width).toEqual(0);
    expect(kanvas.height).toEqual(0);
    expect(kanvas.backgroundColor).toEqual(new Uint8ClampedArray([255, 255, 255, 255]));
    expect(kanvas.fillColor).toEqual(new Uint8ClampedArray([204, 204, 204, 255]));
    expect(kanvas.strokeColor).toEqual(new Uint8ClampedArray([0, 0, 0, 255]));
    expect(ctx).toBeDefined();
    expect(ctx.fillStyle).toBeDefined();
    expect(ctx.clearRect).toBeDefined();
    expect(ctx.fillRect).toBeDefined();
    expect(ctx.beginPath).toBeDefined();
    expect(ctx.moveTo).toBeDefined();
    expect(ctx.lineTo).toBeDefined();
    expect(ctx.stroke).toBeDefined();
  });

  describe('#draw', () => {
    const kanvas = new Canvas();

    it('should not be valid with random string', () => {
      expect(() => {
        kanvas.draw('asd', 'asd', 100, 100)
      }).toThrow('[x, y] must be positive Integer');
    });

    it('should not be valid with negative coordinates', () => {
      expect(() => {
        kanvas.draw(-20, 14, 100, 100)
      }).toThrow('[-20, 14] must be within boundary ([100, 100])');
    });

    it('should not exceed document max resolution', () => {
      expect(() => {
        kanvas.draw(20, 4, 0, 0)
      }).toThrow('[20, 4] must be within boundary ([0, 0])');
    });

    it('can draw a canvas element', () => {
      kanvas.draw(20, 4, 100, 100);
      // calculated with border
      expect(kanvas.element.width).toEqual(22);
      expect(kanvas.element.height).toEqual(6);
      // canvas content
      expect(kanvas.width).toEqual(20);
      expect(kanvas.height).toEqual(4);
    });
  });

  describe('#drawLine', () => {
    const kanvas = new Canvas();

    beforeEach(() => {
      kanvas.draw(20, 4, 100, 100);
    });

    it('should be invalid without any canvas element', () => {
      expect(() => {
        new Canvas().drawLine(1, 2, 6, 2);
      }).toThrow('[0, 0] is not a valid canvas dimensions.');
    });

    it('should not exceed canvas resolution', () => {
      expect(() => {
        kanvas.drawLine(-1, 2, 6, 2);
      }).toThrow('[-1, 2] must be within boundary ([20, 4])');
    });

    test('can draw a line', () => {
      kanvas.drawLine(1, 2, 6, 2);
    });
  });

  describe('#drawRect', () => {
    const kanvas = new Canvas();

    beforeEach(() => {
      kanvas.draw(20, 4, 100, 100);
    });

    it('should be invalid without any canvas element', () => {
      expect(() => {
        new Canvas().drawRect(14, 1, 18, 3);
      }).toThrow('[0, 0] is not a valid canvas dimensions.');
    });

    it('should not exceed canvas resolution', () => {
      expect(() => {
        kanvas.drawRect(14, 1, 21, 3);
      }).toThrow('[21, 3] must be within boundary ([20, 4])');
    });

    it('should not swap top left and bottom right corner', () => {
      expect(() => {
        kanvas.drawRect(18, 3, 14, 1);
      }).toThrow('Invalid rectangle coordinates, upper left corner [18, 3] cannot be larger than or equal to [14, 1]');
    });

    test('can draw a rectangle', () => {
      kanvas.drawRect(14, 1, 18, 3);
    });
  });

  describe('#fill', () => {
    // we need to mimic the values from `fillImageData`
    const kanvas = new Canvas();

    it('matches the snapshot', () => {
      kanvas.draw(20, 4, 100, 100);
      // draw lines and rectangle
      kanvas.drawLine(1, 2, 6, 2);
      kanvas.drawLine(6, 2, 6, 4);
      kanvas.drawRect(14, 1, 18, 3);
      // fill
      kanvas.fill(10, 3, kanvas.fillColor);

      const path = kanvas.ctx.__getEvents();
      expect(path).toMatchSnapshot();
    });
  });
});
