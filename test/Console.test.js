import Canvas from '../src/Canvas';
import Console from '../src/Console';
jest.mock('../src/Canvas');

beforeEach(() => {
  Canvas.mockClear();
});

describe('Console', () => {
  describe('#new', () => {
    it('should initialize canvas', () => {
      expect(Canvas).not.toHaveBeenCalled();
      const a = new Console();
      expect(Canvas).toHaveBeenCalledTimes(1);
    });
  });

  describe('#evaluate', () => {
    // `console` is a reserved keyword
    const konsole = new Console();

    describe('given invalid commands', () => {
      [
        ['', 'Command is invalid: Unsupported command: '],
        ['c', 'Command is invalid: Unsupported command: c'],
        ['invalid', 'Command is invalid: Unsupported command: invalid']
      ].forEach(([input, output]) => {
        it('should give an error message', () => {
          const evaluated = jest.spyOn(konsole, 'evaluate');
          const errorLog = jest.spyOn(konsole, 'errorLog');
          konsole.input = { value: input };
          konsole.evaluate();

          expect(evaluated).toHaveBeenCalled();
          expect(errorLog).toBeCalledWith(output);

          evaluated.mockReset();
          evaluated.mockRestore();
          errorLog.mockReset();
          errorLog.mockRestore();
        });
      });
    });

    describe('given valid commands', () => {
      const mockCanvasInstance = Canvas.mock.instances[0];

      it('should draw a canvas', () => {
        const mockDraw = mockCanvasInstance.draw;
        const evaluated = jest.spyOn(konsole, 'evaluate');
        const cleanup = jest.spyOn(konsole, 'cleanup');
        const errorLog = jest.spyOn(konsole, 'errorLog');
        konsole.input = { value: 'C 200 200' };
        konsole.evaluate();

        expect(evaluated).toHaveBeenCalled();
        expect(errorLog).not.toHaveBeenCalled();

        expect(mockDraw).toHaveBeenCalledWith(200, 200);
        expect(cleanup).toHaveBeenCalled();

        cleanup.mockReset();
        cleanup.mockRestore();
        evaluated.mockReset();
        evaluated.mockRestore();
        errorLog.mockReset();
        errorLog.mockRestore();
      });

      it('should draw a line', () => {
        const mockDrawLine = mockCanvasInstance.drawLine;
        const evaluated = jest.spyOn(konsole, 'evaluate');
        const cleanup = jest.spyOn(konsole, 'cleanup');
        const errorLog = jest.spyOn(konsole, 'errorLog');
        konsole.input = { value: 'L 0 20 60 20' };
        konsole.evaluate();

        expect(evaluated).toHaveBeenCalled();
        expect(errorLog).not.toHaveBeenCalled();

        expect(mockDrawLine).toHaveBeenCalledWith(0, 20, 60, 20);
        expect(cleanup).toHaveBeenCalled();

        cleanup.mockReset();
        cleanup.mockRestore();
        evaluated.mockReset();
        evaluated.mockRestore();
        errorLog.mockReset();
        errorLog.mockRestore();
      });

      it('should draw a rect', () => {
        const mockDrawRect = mockCanvasInstance.drawRect;
        const evaluated = jest.spyOn(konsole, 'evaluate');
        const cleanup = jest.spyOn(konsole, 'cleanup');
        const errorLog = jest.spyOn(konsole, 'errorLog');
        konsole.input = { value: 'R 150 50 180 80' };
        konsole.evaluate();

        expect(evaluated).toHaveBeenCalled();
        expect(errorLog).not.toHaveBeenCalled();

        expect(mockDrawRect).toHaveBeenCalledWith(150, 50, 180, 80);
        expect(cleanup).toHaveBeenCalled();

        cleanup.mockReset();
        cleanup.mockRestore();
        evaluated.mockReset();
        evaluated.mockRestore();
        errorLog.mockReset();
        errorLog.mockRestore();
      });

      it('should fill the canvas', () => {
        const mockFill = mockCanvasInstance.fill;
        const evaluated = jest.spyOn(konsole, 'evaluate');
        const cleanup = jest.spyOn(konsole, 'cleanup');
        const errorLog = jest.spyOn(konsole, 'errorLog');
        konsole.input = { value: 'B 20 20 #F00' };
        konsole.evaluate();

        expect(evaluated).toHaveBeenCalled();
        expect(errorLog).not.toHaveBeenCalled();

        expect(mockFill).toHaveBeenCalledWith(20, 20, new Uint8ClampedArray([255, 0, 0, 255]));
        expect(cleanup).toHaveBeenCalled();

        cleanup.mockReset();
        cleanup.mockRestore();
        evaluated.mockReset();
        evaluated.mockRestore();
        errorLog.mockReset();
        errorLog.mockRestore();
      });

      it('should attempt to quit the program', () => {
        // mock window.confirm
        window.confirm = jest.fn();
        const errorLog = jest.spyOn(konsole, 'errorLog');
        konsole.input = { value: 'Q' };
        konsole.evaluate();

        expect(errorLog).not.toHaveBeenCalled();

        errorLog.mockReset();
        errorLog.mockRestore();
      });
    });
  });
});
