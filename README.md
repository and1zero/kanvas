# Drawing Program Solution with Canvas

[![pipeline status](https://gitlab.com/and1zero/kanvas/badges/master/pipeline.svg)](https://gitlab.com/and1zero/kanvas/commits/master) [![coverage report](https://gitlab.com/and1zero/kanvas/badges/master/coverage.svg)](https://gitlab.com/and1zero/kanvas/commits/master)

## Introduction
Full task instruction can be seen in [drawing_program.txt](drawing_program.txt).
In a nutshell, the program should work as follows:
 1. Create a new canvas
 2. Start drawing on the canvas by issuing various commands
 3. Quit

## CAVEAT
I have no idea if this project is supposed to be done in shell terminal or we are allowed to render said canvas in browser. Since I am more familiar with the latter, that is the method which I'm going with.

Also, because the implementation is in canvas, please make all the arguments in bigger numbers if possible, because it can be pretty hard to see each pixels in browser.

## Installation
I would assume that we are all familiar with Javascript package manager such as [NPM](https://www.npmjs.com/) or [yarn](https://yarnpkg.com/en/). If so, we can proceed with the following commands to install all dependencies:
```bash
$ npm install (or yarn install)
```

Then we can start bundling our codes in `src/` into `dist/bundle.js` by running:
```bash
$ npm build (or yarn build)
```

To watch the files (running in development mode), we can use:
```bash
$ npm start (or yarn start)
```

## Running the tests
To run the test:
``` bash
$ npm test (or yarn test)
```
More information can be found in [test/README.md](test/README.md).

## Running this program
```bash
$ open index.html
```
Just open `index.html` in Chrome or Firefox. If there is any problem, usually refreshing the browser or closing the tab will help.

If there is no input, usually running `npm start` or `yarn start` helps.
