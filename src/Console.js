import Canvas from './Canvas';
import { hexToRgb } from './utils';
/**
 * Console
 * Interpreter to translate command into canvas methods
 */
export default class Console {
  constructor() {
    this.input = document.createElement('input');
    this.input.id = 'console';
    this.input.type = 'text';
    this.input.placeholder = 'Enter command here. Press Enter on your keyboard to execute it.';
    this.log = document.createElement('div');
    this.log.id = 'log';
    // append elements
    document.body.append(this.input);
    document.body.append(this.log);
    // Initialize canvas
    this.canvas = new Canvas();
  }

  clearInput() {
    this.input.value = '';
  }

  clearLog() {
    this.log.style.color = '#000';
    this.log.innerHTML = '';
  }

  cleanup() {
    this.clearInput();
    this.clearLog();
  }

  errorLog(message) {
    this.log.style.color = '#F00';
    this.log.innerHTML = message;
  }

  listen() {
    this.input.addEventListener('keyup', event => {
      this.clearLog();
      if ((event.key || event.code) === 'Enter') {
        // evaluate the command
        this.evaluate();
      }
    });
  }

  evaluate() {
    const command = this.input.value;

    try {
      // maximum constraints might change on every input (e.g on resize)
      const maxHeight = document.documentElement.clientHeight;
      const maxWidth = document.documentElement.clientWidth;

      let arrCommand = command.split(' ');
      const firstCommand = arrCommand.shift();
      // convert coordinate inputs into Integer
      const [x1, y1, x2, y2] = arrCommand.map(arg => +arg);

      switch(firstCommand) {
        case 'Q':
          // Quit
          if (window.confirm('Close program?')) {
            window.close();
          }
          return;
        case 'C':
          // convert string to integers
          // we will be using this technique quite often
          const width = x1;
          const height = y1;

          this.canvas.draw(width, height);
          return this.cleanup();
        case 'L':
          this.canvas.drawLine(x1, y1, x2, y2);
          return this.cleanup();
        case 'R':
          this.canvas.drawRect(x1, y1, x2, y2);
          return this.cleanup();
        case 'B':
          const color = arrCommand[2];
          const rgbColor = hexToRgb(color);
          this.canvas.fill(x1, y1, rgbColor);
          return this.cleanup();
        default:
          throw new Error(`Unsupported command: ${firstCommand}`);
      }
    } catch (error) {
      this.errorLog(`Command is invalid: ${error.message}`);
    }
  }
}
