import {
  drawLineImageData
} from './index';
/**
 * drawRectImageData
 * @param imageData [ImageData] ImageData Object from canvas' 2D context
 * @param x1 [Integer] x-axis of top-left corner
 * @param y1 [Integer] y-axis of top-left corner
 * @param x2 [Integer] x-axis of bottom-right corner
 * @param y2 [Integer] y-axis of bottom-right corner
 * @param strokeColor [Array] Color array in [R, G, B, A]
 * @description This function mutates imageData.data.
 * Ultimately the new imageData will be put inside the canvas
 */
export default function drawRectImageData(imageData, x1, y1, x2, y2, strokeColor) {
  drawLineImageData(imageData, x1, y1, x2, y1, strokeColor);
  drawLineImageData(imageData, x2, y1, x2, y2, strokeColor);
  drawLineImageData(imageData, x2, y2, x1, y2, strokeColor);
  drawLineImageData(imageData, x1, y2, x1, y1, strokeColor);
}
