/**
 * isColorEqual
 * @param imageDataArray [Uint8ClampedArray] Long array of Integer
 * @param currentIndex [Integer] Current index which we want to compare with target index
 * @param targetColor [Array] Target color in [R, G, B, A]
 * @description Comparing sub-section of arrays from imageData.data
 */
export default function isColorEqual(imageDataArray, currentIndex, targetColor) {
  let length = targetColor.length; // must be 4
  let startIndex = currentIndex + length;

  // we are going to compare in reverse:
  // imageDataArray[3] === imageDataArray[7], etc.
  while(length-- && startIndex--) {
    if (Math.abs(imageDataArray[startIndex] - targetColor[length]) > 0) {
      // there is some discrepancy in color
      return false;
    }
  }
  return true;
}
