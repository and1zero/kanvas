import {
  getImageDataIndex
} from './index';

const setColor = (imageData, x, y, strokeColor) => {
  // set pixel's color in imageData.data
  const pixelIndex = getImageDataIndex(imageData.width, x, y);
  let length = strokeColor.length;
  while(length--) {
    imageData.data[pixelIndex + length] = strokeColor[length];
  }
  return;
}

/**
 * drawLineImageData
 * @param imageData [ImageData] ImageData Object from canvas' 2D context
 * @param x1 [Integer]
 * @param y1 [Integer]
 * @param x2 [Integer]
 * @param y2 [Integer]
 * @param strokeColor [Array] Color array in [R, G, B, A]
 * @description This function mutates imageData.data.
 * Draw line from (x1, y1) to (x2, y2) based on Bresenham's line algorithm
 * Ultimately the new imageData will be put inside the canvas
 */
export default function drawLineImageData(imageData, x1, y1, x2, y2, strokeColor) {
  const dx = Math.abs(x2 - x1);
  const sx = x1 < x2 ? 1 : -1;
  const dy = Math.abs(y2 - y1);
  const sy = y1 < y2 ? 1 : -1;
  let err = (dx > dy ? dx : -dy) / 2;

  let e2;
  while(true) {
    setColor(imageData, x1, y1, strokeColor);
    if (x1 === x2 && y1 === y2) break;
    e2 = err;
    if (e2 > -dx) {
      err -= dy;
      x1 += sx;
    }
    if (e2 < dy) {
      err += dx;
      y1 += sy;
    }
  }
}
