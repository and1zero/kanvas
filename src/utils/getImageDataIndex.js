import { PIXEL_SIZE } from './index';
/**
 * getImageDataIndex
 * @param width [Integer]
 * @param x [Integer]
 * @param y [Integer]
 * @param pixelSize [Integer] pixel's size in image data array, it's 4 for [R, G, B, A]
 * @description Helper function to translate x, y coordinate into
 * Uint8ClampedArray index in image data array
 */
export default function getImageDataIndex(width, x, y, pixelSize = PIXEL_SIZE) {
  return pixelSize * (y * width + x);
}
