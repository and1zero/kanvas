// each pixel occupies 4 elements in imageData array [R, G, B, A]
export const PIXEL_SIZE = 4;
// default color: black
export const COLOR_BLACK = new Uint8ClampedArray([0, 0, 0, 255]);
// default color: grey
export const COLOR_GREY = new Uint8ClampedArray([204, 204, 204, 255]);
// default color: transparent
export const COLOR_TRANSPARENT = new Uint8ClampedArray([0, 0, 0, 0]);
// default color: white
export const COLOR_WHITE = new Uint8ClampedArray([255, 255, 255, 255]);
