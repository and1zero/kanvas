/**
 * hexToRgb
 * @param hex [String]
 * @description Convert hex color codes (#FFFFFF) to its rgba counterpart (rgba(255, 255, 255, 255)).
 * This is necessary because Javascript canvas only returns image layer data in RGBA
 * (it supports transparency), but input color can only support hexadecimal.
 * @return [Uint8ClampedArray] of [R, G, B, A], where the range of each R, G, B, A is 0-255
 */
export default function hexToRgb(hex) {
  const shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
  hex = hex.replace(shorthandRegex, function(m, r, g, b) {
    return r + r + g + g + b + b;
  });

  const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
  const array = result ? [
    parseInt(result[1], 16),
    parseInt(result[2], 16),
    parseInt(result[3], 16),
    255
  ] : [255, 255, 255, 255];
  // convert it into proper Uint8ClampedArray
  return new Uint8ClampedArray(array);
}
