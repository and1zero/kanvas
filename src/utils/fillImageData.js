import {
  PIXEL_SIZE,
  getImageDataIndex,
  isColorEqual
} from './index';

/**
 * fillImageData
 * @param imageData [ImageData] ImageData Object from canvas' 2D context
 * @param x [Integer] x-axis of starting coordinate
 * @param y [Integer] y-axis of starting coordinate
 * @param fillColor [Array] Color array in [R, G, B, A]
 * @description This function mutates imageData.data.
 * Ultimately the new imageData will be put inside the canvas
 */
export default function fillImageData(imageData, x, y, fillColor) {
  const width = imageData.width;
  const height = imageData.height;
  // a function to translate coordinate to an index in imageData.data
  const getIndex = (a, b) => {
    return getImageDataIndex(width, a, b);
  };

  const startingIndex = getIndex(x, y); // R color for target pixel

  if (isColorEqual(imageData.data, startingIndex, fillColor)) {
    // trying to fill the same color
    return;
  }

  // get the [R, G, B, A]
  const startingColor = imageData.data.slice(startingIndex, startingIndex + PIXEL_SIZE);
  // up, right, left, down
  const steps = [[1, 0], [0, 1], [-1, 0], [0, -1]];
  let stack = [[x, y]];
  let seen = {};
  let key,
    currentX,
    currentY,
    currentIndex,
    i,
    nextX,
    nextY;

  while (stack.length > 0) {
    [currentX, currentY] = stack.pop();
    currentIndex = getIndex(currentX, currentY);

    // if this pixel is not the same color with starting color, move to the next iteration
    if (!isColorEqual(imageData.data, currentIndex, startingColor)) {
      continue;
    }

    // update pixel to target color and check its neighbor
    i = steps.length;
    while(i--) {
      // Utilizing the same loop to set the RGBA color
      if (i < PIXEL_SIZE) {
        imageData.data[currentIndex + i] = fillColor[i];
      }

      // get new coordinate while adjusting x and y based on current step
      nextX = currentX + steps[i][0];
      nextY = currentY + steps[i][1];
      key = `${nextX},${nextY}`;

      // if new coordinate is out of boundary or have been seen, skip it
      if (nextX < 0 || nextY < 0 || nextX >= width || nextY >= height || seen[key]) {
        continue;
      }

      // Push prospecting coordinates into stack and seen
      stack.push([nextX, nextY]);
      seen[key] = true;
    }
  }
}
