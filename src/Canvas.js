import {
  COLOR_BLACK,
  COLOR_GREY,
  COLOR_WHITE,
  drawLineImageData,
  drawRectImageData,
  fillImageData
} from './utils';
/**
 * Canvas
 * Manipulate canvas DOM
 */
export default class Canvas {
  constructor() {
    this.element = document.createElement('canvas');
    this.element.id = 'canvas';
    this.element.width = 0;
    this.element.height = 0;
    this.width = 0;
    this.height = 0;
    document.body.appendChild(this.element);
    this.ctx = this.element.getContext('2d');

    this.backgroundColor = COLOR_WHITE;
    this.fillColor = COLOR_GREY;
    this.strokeColor = COLOR_BLACK;
  }

  /**
   * draw
   * @description Draw the initial canvas
   * We are passing optional maxWidth and maxHeight for testing
   */
  draw(
    width,
    height,
    maxWidth=document.documentElement.clientWidth,
    maxHeight=document.documentElement.clientHeight
  ) {
    if (this._validateBoundaries(width, height, maxWidth, maxHeight)) {
      // we need to create the element + border
      this.element.width = width + 2;
      this.element.height = height + 2;
      // refers to the width and height of canvas content
      this.width = width;
      this.height = height;
      // Fill background to make the resize visible
      // by default canvas will have its background as rgb(0, 0, 0, 0) or black transparent
      let imageData = this.ctx.createImageData(this.element.width, this.element.height);
      // fill with white backgroundColor
      imageData.data.fill(255);
      this.ctx.putImageData(imageData, 0, 0);
      // add the border
      this.drawRect(0, 0, this.width + 1, this.height + 1, this.element.width, this.element.height);
    }
  }

  /**
   * drawLine
   * @description Drawing a line from (x1, y1) to (x2, y2)
   */
  drawLine(x1, y1, x2, y2) {
    this._validateCanvas();

    if (this._validateCanvasBoundaries(x1, y1) && this._validateCanvasBoundaries(x2, y2)) {
      // default stroke of HTML canvas is not precise at all
      // so we need to put in the image data manually
      // imageData will be mutated
      let imageData = this.ctx.getImageData(0, 0, this.element.width, this.element.height);
      drawLineImageData(imageData, x1, y1, x2, y2, this.strokeColor);
      // replace imageData with "lines" image data
      this.ctx.putImageData(imageData, 0, 0);
    }
  }

  /**
   * drawRect
   * @description Drawing a rectangle from (x1, y1) where (x1, y1) is the top-left corner
   * and (x2, y2) is the bottom-right corner
   */
  drawRect(x1, y1, x2, y2, maxWidth=this.width, maxHeight=this.height) {
    this._validateCanvas();

    if (x2 <= x1 || y2 <= y1) {
      throw new Error(`Invalid rectangle coordinates, upper left corner [${x1}, ${y1}] cannot be larger than or equal to [${x2}, ${y2}]`)
    }

    if (this._validateBoundaries(x1, y1, maxWidth, maxHeight) && this._validateBoundaries(x2, y2, maxWidth, maxHeight)) {
      // default stroke of HTML canvas is not precise at all
      // so we need to put in the image data manually
      // imageData will be mutated
      let imageData = this.ctx.getImageData(0, 0, this.element.width, this.element.height);
      drawRectImageData(imageData, x1, y1, x2, y2, this.strokeColor);
      // replace imageData with "lines" image data
      this.ctx.putImageData(imageData, 0, 0);
    }
  }

  /**
   * @method fill
   * @param x [Integer]
   * @param y [Integer]
   * @param color [Array] Array of [R, G, B, A]
   * @description the pure function for fill is inside `utils/fillImageData`.
   */
  fill(x, y, color=this.fillColor) {
    this._validateCanvas();

    if (this._validateCanvasBoundaries(x, y)) {
      // imageData will be mutated after `fillImageData`
      let imageData = this.ctx.getImageData(0, 0, this.element.width, this.element.height);
      fillImageData(imageData, x, y, color);
      // replace imageData with "filled" image data
      this.ctx.putImageData(imageData, 0, 0);
    }
  }

  /* private */
  _validateBoundaries(x, y, maxWidth, maxHeight) {
    if (isNaN(x) || isNaN(y)) {
      throw new Error(`[x, y] must be positive Integer`);
    }

    if (x < 0 || y < 0 || x > maxWidth || y > maxHeight) {
      throw new Error(`[${x}, ${y}] must be within boundary ([${maxWidth}, ${maxHeight}])`);
    }
    return true;
  }

  _validateCanvas() {
    if (this.element.width <= 0 || this.element.height <= 0) {
      throw new Error(`[${this.element.width}, ${this.element.height}] is not a valid canvas dimensions.`);
    }
  }

  _validateCanvasBoundaries(x, y) {
    const maxWidth = this.width;
    const maxHeight = this.height;

    return this._validateBoundaries(x, y, maxWidth, maxHeight);
  }
}
