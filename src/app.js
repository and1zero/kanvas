import Console from './Console';

/**
 * app.js
 * @author Andi Susanto
 * Entrypoint of the application
 */
new Console().listen();
